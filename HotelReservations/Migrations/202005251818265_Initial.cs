﻿namespace HotelReservations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bookings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.Int(nullable: false),
                        EndDate = c.Int(nullable: false),
                        RoomId = c.Int(),
                        Status = c.Int(nullable: false),
                        CreationTime = c.DateTimeOffset(nullable: false, precision: 7),
                        CreatorUserId = c.Int(nullable: false),
                        LastModificationTime = c.DateTimeOffset(precision: 7),
                        LastModificationUserId = c.Int(),
                        DeletionTime = c.DateTimeOffset(precision: 7),
                        DeleterUserId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rooms", t => t.RoomId)
                .Index(t => t.RoomId);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsAvailble = c.Boolean(nullable: false),
                        CreationTime = c.DateTimeOffset(nullable: false, precision: 7),
                        CreatorUserId = c.Int(nullable: false),
                        LastModificationTime = c.DateTimeOffset(precision: 7),
                        LastModificationUserId = c.Int(),
                        DeletionTime = c.DateTimeOffset(precision: 7),
                        DeleterUserId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bookings", "RoomId", "dbo.Rooms");
            DropIndex("dbo.Bookings", new[] { "RoomId" });
            DropTable("dbo.Rooms");
            DropTable("dbo.Bookings");
        }
    }
}
