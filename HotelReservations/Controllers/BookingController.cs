﻿using AutoMapper;
using HotelReservations.Models;
using HotelReservations.Models.Dtos;
using HotelReservations.Models.Enums;
using HotelReservations.Repositories;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace HotelReservations.Controllers
{
    public class BookingController : ApiController
    {
        private readonly ModelContext db = new ModelContext();

        private readonly UnitOfWork _unitOfWork = new UnitOfWork();

        private readonly int _startDateForBooking = 0;

        private readonly int _endDateForBooking = 365;

        // GET: api/Bookings
        public IQueryable<Booking> GetBookings()
        {
            return db.Bookings;
        }

        // GET: api/Bookings/5
        [ResponseType(typeof(Booking))]
        public IHttpActionResult GetBooking(int id)
        {
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return NotFound();
            }

            return Ok(booking);
        }

        // POST: api/Bookings
        [ResponseType(typeof(Booking))]
        public IHttpActionResult PostBooking(BookingDto input)
        {
            try
            {
                //Init local variables
                (bool, int?) roomIsFree = (false, null);
                var dateIsValid = false;

                //Check if Date is valid
                dateIsValid = CheckIfDateIsValid(input);
                if (dateIsValid)
                    roomIsFree = CheckIfRoomIsFree(input);

                //Map input BookingDto to Db Booking
                var config = new MapperConfiguration(cfg => cfg.CreateMap<BookingDto, Booking>());
                var booking = new Booking();
                var mapper = config.CreateMapper();
                mapper.Map(input, booking);

                booking.Status = roomIsFree.Item1 ? BookingStatus.Acepted : BookingStatus.Rejected;
                booking.RoomId = roomIsFree.Item2;

                _unitOfWork.BookingRepository.Insert(booking);
                _unitOfWork.Save();
            }
            catch (DataException dex)
            {
                ModelState.AddModelError("", "Unable to save changes. " + dex.Message);
            }
            return StatusCode(HttpStatusCode.Created);
        }

        // DELETE: api/Bookings/5
        [ResponseType(typeof(Booking))]
        public IHttpActionResult DeleteBooking(int id)
        {
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return NotFound();
            }

            db.Bookings.Remove(booking);
            db.SaveChanges();

            return Ok(booking);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private (bool, int?) CheckIfRoomIsFree(BookingDto input)
        {
            var rooms = _unitOfWork.RoomRepository.Get().ToList();
            var isAvailable = false;
            int? roomId = null;
            foreach (var room in rooms)
            {
                var roomIsBooked = _unitOfWork.BookingRepository.Get(b => b.StartDate <= input.EndDate && b.EndDate >= input.StartDate)
                                                                 .Where(b => b.Status == BookingStatus.Acepted && b.RoomId == room.Id).ToList().Any();
                if (!roomIsBooked)
                {
                    isAvailable = true;
                    roomId = room.Id;
                    break;
                }
            }
            return (isAvailable, roomId);
        }

        private bool CheckIfDateIsValid(BookingDto input)
        {
            if ((input.StartDate >= _startDateForBooking && input.EndDate <= _endDateForBooking) && input.StartDate <= input.EndDate)
            {
                return true;
            }
            return false;
        }
    }
}