﻿using HotelReservations.Models;
using System.Data.Entity;

namespace HotelReservations
{
    public class ModelContext : DbContext
    {
        public ModelContext() : base("name = dbconnection")
        {
        }

        public DbSet<Room> Rooms { get; set; }

        public DbSet<Booking> Bookings { get; set; }
    }
}