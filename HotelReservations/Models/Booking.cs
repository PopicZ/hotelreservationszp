﻿using HotelReservations.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace HotelReservations.Models
{
    public class Booking : BaseModel
    {
        [Required]
        public int StartDate { get; set; }

        public int EndDate { get; set; }

        public Room Room { get; set; }

        public int? RoomId { get; set; }

        public BookingStatus Status { get; set; }
    }
}