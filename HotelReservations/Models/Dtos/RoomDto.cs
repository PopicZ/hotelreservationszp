﻿using System.Collections.Generic;

namespace HotelReservations.Models.Dtos
{
    public class RoomDto : BaseModelDto
    {
        public string Name { get; set; }

        public bool IsAvailble { get; set; }

        public ICollection<BookingDto> Bookings { get; set; }
    }
}