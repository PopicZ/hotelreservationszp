﻿namespace HotelReservations.Models.Dtos
{
    public class BookingDto : BaseModelDto
    {
        public int StartDate { get; set; }

        public int EndDate { get; set; }

        public int? RoomId { get; set; }
    }
}