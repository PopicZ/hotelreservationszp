﻿using System;

namespace HotelReservations.Models.Dtos
{
    public class BaseModelDto
    {
        public int Id { get; set; }

        public int CreatorUserId { get; set; }

        public DateTimeOffset? LastModificationTime { get; set; }

        public int? LastModificationUserId { get; set; }

        public DateTimeOffset? DeletionTime { get; set; }

        public int? DeleterUserId { get; set; }

        public bool IsDeleted { get; set; }
    }
}