﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HotelReservations.Models
{
    public abstract class BaseModel
    {
        [Required]
        public int Id { get; set; }

        public DateTimeOffset CreationTime { get; set; } = DateTimeOffset.Now;

        public int CreatorUserId { get; set; }

        public DateTimeOffset? LastModificationTime { get; set; }

        public int? LastModificationUserId { get; set; }

        public DateTimeOffset? DeletionTime { get; set; }

        public int? DeleterUserId { get; set; }

        public bool IsDeleted { get; set; }
    }
}