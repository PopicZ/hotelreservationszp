﻿using System.Collections.Generic;

namespace HotelReservations.Models
{
    public class Room : BaseModel
    {
        public string Name { get; set; }

        public bool IsAvailble { get; set; }

        public ICollection<Booking> Bookings { get; set; }
    }
}