﻿using HotelReservations.Models;
using System;

namespace HotelReservations.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private ModelContext context = new ModelContext();
        private GenericRepository<Booking> bookingRepository;
        private GenericRepository<Room> roomRepository;

        public GenericRepository<Booking> BookingRepository
        {
            get
            {
                if (this.bookingRepository == null)
                {
                    this.bookingRepository = new GenericRepository<Booking>(context);
                }
                return bookingRepository;
            }
        }

        public GenericRepository<Room> RoomRepository
        {
            get
            {
                if (this.roomRepository == null)
                {
                    this.roomRepository = new GenericRepository<Room>(context);
                }
                return roomRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}