﻿using HotelReservations.Models;
using HotelReservations.Repositories;

namespace HotelReservations.Services
{
    public class BookingService
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public void InsertBooking(Booking booking)
        {
            unitOfWork.BookingRepository.Insert(booking);
            unitOfWork.Save();
        }
    }
}