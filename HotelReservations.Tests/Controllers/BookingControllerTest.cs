﻿using HotelReservations.Controllers;
using HotelReservations.Models;
using HotelReservations.Models.Dtos;
using HotelReservations.Models.Enums;
using HotelReservations.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace HotelReservations.Tests.Controllers
{
    [TestClass]
    public class BookingControllerTest
    {
        private readonly UnitOfWork _unitOfWork = new UnitOfWork();
        private readonly BookingController _controller = new BookingController();

        [TestMethod]
        public void TestWrongInput()
        {
            #region Seed Room

            var Room1 = new Room()
            {
                Name = "Room1",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room1);
            _unitOfWork.Save();

            #endregion Seed Room

            //Date in past
            var Test1 = new BookingDto()
            {
                StartDate = -4,
                EndDate = 2,
                CreatorUserId = 1,
                IsDeleted = false
            };

            var action1 = _controller.PostBooking(Test1);
            var bookingResult1 = _unitOfWork.BookingRepository.Get().FirstOrDefault();
            Assert.AreEqual(BookingStatus.Rejected, bookingResult1.Status);

            //Date > 365 + today
            var Test2 = new BookingDto()
            {
                StartDate = 200,
                EndDate = 400,
                CreatorUserId = 1,
                IsDeleted = false
            };

            var action2 = _controller.PostBooking(Test2);
            var bookingResult2 = _unitOfWork.BookingRepository.Get().FirstOrDefault();
            Assert.AreEqual(BookingStatus.Rejected, bookingResult2.Status);

            Cleaner();
        }

        [TestMethod]
        public void TestRequestsAccepted()
        {
            #region Seed Rooms

            var Room1 = new Room()
            {
                Name = "Room1",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room1);
            _unitOfWork.Save();

            var Room2 = new Room()
            {
                Name = "Room2",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room2);
            _unitOfWork.Save();

            var Room3 = new Room()
            {
                Name = "Room3",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room3);
            _unitOfWork.Save();

            #endregion Seed Rooms

            #region Seed Bookings

            var Test2 = new List<BookingDto>
            {
                // 0 5 Acccept
                new BookingDto()
                {
                    StartDate = 0,
                    EndDate = 5,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 7 13 Accept
                new BookingDto()
                {
                    StartDate = 7,
                    EndDate = 13,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 3 9 Accept
                new BookingDto()
                {
                    StartDate = 3,
                    EndDate = 9,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 5 7 Decline
                new BookingDto()
                {
                    StartDate = 5,
                    EndDate = 7,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                 // 6 6 Accept
                 new BookingDto()
                {
                    StartDate = 6,
                    EndDate = 6,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 0 4 Accept
                new BookingDto()
                {
                    StartDate = 0,
                    EndDate = 4,
                    CreatorUserId = 1,
                    IsDeleted = false
                }
            };

            #endregion Seed Bookings

            foreach (var booking in Test2)
            {
                var action = _controller.PostBooking(booking);
                var bookingResult = _unitOfWork.BookingRepository.Get().LastOrDefault();
                Assert.AreEqual(BookingStatus.Acepted, bookingResult.Status);
            }

            Cleaner();
        }

        [TestMethod]
        public void TestsRequestDeclined()
        {
            #region Seed Rooms

            var Room1 = new Room()
            {
                Name = "Room1",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room1);
            _unitOfWork.Save();

            var Room2 = new Room()
            {
                Name = "Room2",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room2);
            _unitOfWork.Save();

            var Room3 = new Room()
            {
                Name = "Room3",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room3);
            _unitOfWork.Save();

            #endregion Seed Rooms

            #region Seed Bookings

            var Test3 = new List<BookingDto>
            {
                // 1 3 Acccept
                new BookingDto()
                {
                    StartDate = 1,
                    EndDate = 3,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 2 5 Accept
                new BookingDto()
                {
                    StartDate = 2,
                    EndDate = 5,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 1 9 Accept
                new BookingDto()
                {
                    StartDate = 1,
                    EndDate = 9,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 0 15 Decline
                new BookingDto()
                {
                    StartDate = 0,
                    EndDate = 15,
                    CreatorUserId = 1,
                    IsDeleted = false
                }
            };

            #endregion Seed Bookings

            var i = 1;
            foreach (var booking in Test3)
            {
                var action = _controller.PostBooking(booking);
                var bookingResult = _unitOfWork.BookingRepository.Get().LastOrDefault();
                if (i == 4)
                    Assert.AreEqual(BookingStatus.Rejected, bookingResult.Status);
                else
                    Assert.AreEqual(BookingStatus.Acepted, bookingResult.Status);
                i++;
            }

            Cleaner();
        }

        [TestMethod]
        public void TestAfterADecline()
        {
            #region Seed Rooms

            var Room1 = new Room()
            {
                Name = "Room1",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room1);
            _unitOfWork.Save();

            var Room2 = new Room()
            {
                Name = "Room2",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room2);
            _unitOfWork.Save();

            var Room3 = new Room()
            {
                Name = "Room3",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room3);
            _unitOfWork.Save();

            #endregion Seed Rooms

            #region Seed Bookings

            var Test4 = new List<BookingDto>
            {
                // 1 3 Acccept
                new BookingDto()
                {
                    StartDate = 1,
                    EndDate = 3,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 0 15 Accept
                new BookingDto()
                {
                    StartDate = 0,
                    EndDate = 15,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 1 9 Accept
                new BookingDto()
                {
                    StartDate = 1,
                    EndDate = 9,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 2 5 Decline
                new BookingDto()
                {
                    StartDate = 2,
                    EndDate = 5,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 4 9 Accept
                new BookingDto()
                {
                    StartDate = 4,
                    EndDate = 9,
                    CreatorUserId = 1,
                    IsDeleted = false
                }
            };

            #endregion Seed Bookings

            var i = 1;
            foreach (var booking in Test4)
            {
                var action = _controller.PostBooking(booking);
                var bookingResult = _unitOfWork.BookingRepository.Get().LastOrDefault();
                if (i == 4)
                    Assert.AreEqual(BookingStatus.Rejected, bookingResult.Status);
                else
                    Assert.AreEqual(BookingStatus.Acepted, bookingResult.Status);
                i++;
            }

            Cleaner();
        }

        //This test is diffrent then in document, Bookink 3 and Booking 5
        //are Rejected and Booking 8 is Accepted
        [TestMethod]
        public void TestRequestComplex()
        {
            #region Seed Rooms

            var Room1 = new Room()
            {
                Name = "Room1",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room1);
            _unitOfWork.Save();

            var Room2 = new Room()
            {
                Name = "Room2",
                IsAvailble = true,
                CreatorUserId = 1,
                IsDeleted = false
            };

            _unitOfWork.RoomRepository.Insert(Room2);
            _unitOfWork.Save();

            #endregion Seed Rooms

            #region Seed Booking

            var Test5 = new List<BookingDto>
            {
                // 1 3 Accept
                new BookingDto()
                {
                    StartDate = 1,
                    EndDate = 3,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 0 4 Accept
                new BookingDto()
                {
                    StartDate = 0,
                    EndDate = 4,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 2 3 Decline
                new BookingDto()
                {
                    StartDate = 2,
                    EndDate = 3,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 5 5 Accept
                new BookingDto()
                {
                    StartDate = 5,
                    EndDate = 5,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                // 4 10 Decline
                 new BookingDto()
                {
                    StartDate = 4,
                    EndDate = 10,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                 // 10 10 Accept
                new BookingDto()
                {
                    StartDate = 10,
                    EndDate = 10,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                 // 6 7 Accept
                new BookingDto()
                {
                    StartDate = 6,
                    EndDate = 7,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                 // 8 10 Accept
                new BookingDto()
                {
                    StartDate = 8,
                    EndDate = 10,
                    CreatorUserId = 1,
                    IsDeleted = false
                },
                 // 8 9 Accept
                new BookingDto()
                {
                    StartDate = 8,
                    EndDate = 9,
                    CreatorUserId = 1,
                    IsDeleted = false
                }
            };

            #endregion Seed Booking

            var i = 1;

            foreach (var booking in Test5)
            {
                var action = _controller.PostBooking(booking);
                var bookingResult = _unitOfWork.BookingRepository.Get().LastOrDefault();
                if (i == 3 || i == 5)
                    Assert.AreEqual(BookingStatus.Rejected, bookingResult.Status);
                else
                    Assert.AreEqual(BookingStatus.Acepted, bookingResult.Status);
                i++;
            }

            Cleaner();
        }

        private void Cleaner()
        {
            var bookings = _unitOfWork.BookingRepository.Get().ToList();
            var rooms = _unitOfWork.RoomRepository.Get().ToList();
            _unitOfWork.BookingRepository.DeleteRange(bookings);
            _unitOfWork.RoomRepository.DeleteRange(rooms);
            _unitOfWork.Save();
        }
    }
}